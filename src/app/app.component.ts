import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'app';

  Username: String;
  Email: String;
  Endereco: String;
  Password: String;

  salvar(){
    alert("Cadastro Realizado!");
  }

  limpar(){
    this.Username = '';
    this.Email = '';
    this.Endereco = '';
    this.Password = '';
  }
}
